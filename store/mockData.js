userInfo: {
		avatar: 'https://cmxishuashua.oss-cn-shenzhen.aliyuncs.com/20150727151430_E5CS4.png',
		nickname: '洗刷刷'
	},
	addrSelected: {
		gName: '阿智1',
		gGender: '先生',
		gPhone: '18077324882',
		addrArea: '彰泰天街1',
		addrDetail: '3栋4单元7023栋4单元7023栋4单元7023栋4单元702',
	},
	addrlist: [{
		selected: true,
		gName: '阿智1',
		gGender: '先生',
		gPhone: '18077324882',
		addrArea: '彰泰天街1',
		addrDetail: '3栋4单元702',
	}, {
		selected: false,
		gName: '阿智2',
		gGender: '先生',
		gPhone: '18077324882',
		addrArea: '彰泰天街2',
		addrDetail: '3栋4单元7023栋4单元7023栋4单元7023栋4单元702',
	}],
	orderList: {
		dynamic: [{
				id: 100021,
				type: '衣物干洗',
				orderId: '2209888998108901',
				status: 4, 
				createDate: '2019/12/01 09:30',
				pickupTime: '2019/12/02 10:21',
				address: '彰泰天街 3栋4单元702',
				num: 6,
				price: 99.03,
				detail: [{
						category: '大衣',
						subdivision: '长款',
						num: 2,
						color: '红色',
						remark: '有破损',
						price: 45.00
					},
					{
						category: '羽绒服',
						subdivision: '中款',
						num: 1,
						color: '白色',
						remark: '油渍',
						price: 30.00
					},
					{
						category: '大衣',
						subdivision: '长款',
						num: 1,
						color: '红色',
						remark: '',
						price: 30.00,
					}
				]
			},
			{
				id: 100022,
				type: '衣物干洗',
				orderId: '2209888998108902',
				status: 1,
				createDate: '2019/12/01 09:30',
				pickupTime: '2019/12/03 10:21',
				address: '彰泰天街 3栋4单元702',
				num: 3,
				price: 99.00,
				detail: [

				]
			}
		],
		orderHistory: [{
				id: 100051,
				type: '衣物干洗',
				orderId: '2209888998108901',
				status: 5,
				createDate: '2019/12/01 09:30',
				pickupTime: '2019/12/02 10:21',
				address: '彰泰天街 3栋4单元702',
				num: 6,
				price: 99.03,
				detail: [{
						category: '大衣',
						subdivision: '长款',
						num: 2,
						color: '红色',
						remark: '有破损',
						price: 45.00
					},
					{
						category: '羽绒服',
						subdivision: '中款',
						num: 1,
						color: '白色',
						remark: '油渍',
						price: 30.00
					},
					{
						category: '大衣',
						subdivision: '长款',
						num: 1,
						color: '红色',
						remark: '',
						price: 30.00,
					}
				]
			},
			{
				id: 100052,
				type: '衣物干洗',
				orderId: '2209888998108901',
				status: 5,
				createDate: '2019/12/01 09:30',
				pickupTime: '2019/12/02 10:21',
				address: '彰泰天街 3栋4单元702',
				num: 6,
				price: 99.03,
				detail: [{
						category: '大衣',
						subdivision: '长款',
						num: 2,
						color: '红色',
						remark: '有破损',
						price: 45.00
					},
					{
						category: '羽绒服',
						subdivision: '中款',
						num: 1,
						color: '白色',
						remark: '油渍',
						price: 30.00
					},
					{
						category: '大衣',
						subdivision: '长款',
						num: 1,
						color: '红色',
						remark: '',
						price: 30.00,
					}
				]
			},
			{
				id: 100053,
				type: '衣物干洗',
				orderId: '2209888998108901',
				status: 5,
				createDate: '2019/12/01 09:30',
				pickupTime: '2019/12/02 10:21',
				address: '彰泰天街 3栋4单元702',
				num: 6,
				price: 99.03,
				detail: [{
						category: '大衣',
						subdivision: '长款',
						num: 2,
						color: '红色',
						remark: '有破损',
						price: 45.00
					},
					{
						category: '羽绒服',
						subdivision: '中款',
						num: 1,
						color: '白色',
						remark: '油渍',
						price: 30.00
					},
					{
						category: '大衣',
						subdivision: '长款',
						num: 1,
						color: '红色',
						remark: '',
						price: 30.00,
					}
				]
			},
			{
				id: 100054,
				type: '衣物干洗',
				orderId: '2209888998108901',
				status: 5,
				createDate: '2019/12/01 09:30',
				pickupTime: '2019/12/02 10:21',
				address: '彰泰天街 3栋4单元702',
				num: 6,
				price: 99.03,
				detail: [{
						category: '大衣',
						subdivision: '长款',
						num: 2,
						color: '红色',
						remark: '有破损',
						price: 45.00
					},
					{
						category: '羽绒服',
						subdivision: '中款',
						num: 1,
						color: '白色',
						remark: '油渍',
						price: 30.00
					},
					{
						category: '大衣',
						subdivision: '长款',
						num: 1,
						color: '红色',
						remark: '',
						price: 30.00,
					}
				]
			},
			{
				id: 100055,
				type: '衣物干洗',
				orderId: '2209888998108901',
				status: 5,
				createDate: '2019/12/01 09:30',
				pickupTime: '2019/12/02 10:21',
				address: '彰泰天街 3栋4单元702',
				num: 6,
				price: 99.03,
				detail: [{
						category: '大衣',
						subdivision: '长款',
						num: 2,
						color: '红色',
						remark: '有破损',
						price: 45.00
					},
					{
						category: '羽绒服',
						subdivision: '中款',
						num: 1,
						color: '白色',
						remark: '油渍',
						price: 30.00
					},
					{
						category: '大衣',
						subdivision: '长款',
						num: 1,
						color: '红色',
						remark: '',
						price: 30.00,
					}
				]
			},
			{
				id: 100056,
				type: '衣物干洗',
				orderId: '2209888998108901',
				status: 5,
				createDate: '2019/12/01 09:30',
				pickupTime: '2019/12/02 10:21',
				address: '彰泰天街 3栋4单元702',
				num: 6,
				price: 99.03,
				detail: [{
						category: '大衣',
						subdivision: '长款',
						num: 2,
						color: '红色',
						remark: '有破损',
						price: 45.00
					},
					{
						category: '羽绒服',
						subdivision: '中款',
						num: 1,
						color: '白色',
						remark: '油渍',
						price: 30.00
					},
					{
						category: '大衣',
						subdivision: '长款',
						num: 1,
						color: '红色',
						remark: '',
						price: 30.00,
					}
				]
			}
		]
	},
	rechargeHistory: [
		{
			id: 100066,
			orderId: '2209888998108901',
			type: '充值',
			createDate: '2019/12/01 09:30',
			status: "未支付",
			way: '余额',
			amount: 99.00,
		},
		{
			id: 100067,
			orderId: '2209888998108901',
			type: '支付订单',
			createDate: '2019/12/01 09:30',
			status: "已支付",
			way: '微信支付',
			amount: 2000.55,
		},
		{
			id: 100068,
			orderId: '2209888998108901',
			type: '充值',
			createDate: '2019/12/01 09:30',
			status: "已支付",
			way: '微信支付',
			amount: 99.00,
		},
		{
			id: 100069,
			orderId: '2209888998108901',
			type: '充值',
			createDate: '2019/12/01 09:30',
			status: "已支付",
			way: '微信支付',
			amount: 99.00,
		},
		{
			id: 100061,
			orderId: '2209888998108901',
			type: '支付订单',
			createDate: '2019/12/01 09:30',
			status: "已支付",
			way: '微信支付',
			amount: 99.00,
		},
		{
			id: 100062,
			orderId: '2209888998108901',
			type: '支付订单',
			createDate: '2019/12/01 09:30',
			status: "已支付",
			way: '微信支付',
			amount: 99.00,
		},
		{
			id: 100063,
			orderId: '2209888998108901',
			type: '支付订单',
			createDate: '2019/12/01 09:30',
			status: "已支付",
			way: '微信支付',
			amount: 99.00,
		}
	]