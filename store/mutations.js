export default {
	setTimeLine(state, data) {
		state.timeLineData = [...data]
	},
	updateCalendarItemsPerPage(state, itemNumbers) {
		state.calendarItemsPerPage = itemNumbers
	},
	updateFinalTime(state, timeData) {
		state.finalTime = timeData
	},
	updateIsLogin(state, boolValue) {
		state.isLogin = boolValue
	},
	updateUserInfo(state, userInfo) {
		state.userInfo = userInfo
	}
}
